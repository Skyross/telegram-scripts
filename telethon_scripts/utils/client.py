# -*- coding: utf-8 -*-
from typing import Optional

from alchemysession import AlchemySessionContainer
from pydantic import BaseSettings
from sqlalchemy import create_engine
from telethon import TelegramClient

from .constants import SESSION_TABLE_PREFIX
from .database import PostgresqlSettings

__all__ = ("create_telegram_client",)


class TelegramSettings(BaseSettings):
    api_id: Optional[int]
    api_hash: Optional[str]
    flood_sleep_threshold: Optional[int] = None

    class Config:
        env_prefix = "TELEGRAM_"
        allow_mutation = False
        anystr_strip_whitespace = True


def create_telegram_client(
    session_name: str,
    api_id: Optional[int] = None,
    api_hash: Optional[str] = None,
    flood_sleep_threshold: Optional[int] = None,
) -> TelegramClient:
    pg_settings = PostgresqlSettings()
    engine = create_engine(
        url=pg_settings.as_url("postgresql+psycopg2"),
        connect_args={"application_name": session_name},
    )
    container = AlchemySessionContainer(
        engine=engine, table_prefix=SESSION_TABLE_PREFIX
    )

    telegram_settings = TelegramSettings(
        api_id=api_id,
        api_hash=api_hash,
        flood_sleep_threshold=flood_sleep_threshold,
    )

    client = TelegramClient(
        container.new_session(session_name),
        **telegram_settings.dict(skip_defaults=True, exclude_none=True),
    )

    return client
