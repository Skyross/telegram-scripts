# -*- coding: utf-8 -*-
import logging
from typing import Optional

from pydantic import BaseSettings

logger = logging.getLogger(__name__)


class PostgresqlSettings(BaseSettings):
    host: str
    port: int
    db: str
    user: str
    password: str

    class Config:
        env_prefix = "POSTGRES_"
        allow_mutation = False
        anystr_strip_whitespace = True

    def as_url(self, dialect: Optional[str] = None) -> str:
        return (
            f"{dialect or 'postgresql'}://{self.user}:{self.password}"
            f"@{self.host}:{self.port}/{self.db}"
        )
