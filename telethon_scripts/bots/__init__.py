# -*- coding: utf-8 -*-
import asyncclick as click


@click.group()
@click.option(
    "--api-id", "-i", type=int, prompt=True, envvar="TELEGRAM_API_ID", show_envvar=True
)
@click.option(
    "--api-hash", "-h", prompt=True, envvar="TELEGRAM_API_HASH", show_envvar=True
)
@click.pass_context
async def main(ctx: click.Context, api_id: int, api_hash: str, bot_token: str) -> None:
    """
    Utility to manage Telegram bots.
    """
    # ensure that ctx.obj exists and is a dict (in case `cli()` is called
    # by means other than the `if` block below)
    ctx.ensure_object(dict)

    ctx.obj["api_id"] = api_id
    ctx.obj["api_hash"] = api_hash
