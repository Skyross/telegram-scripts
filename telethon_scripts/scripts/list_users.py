# -*- coding: utf-8 -*-
import logging

import asyncclick as click
from telethon import TelegramClient
from telethon.tl import types

from telethon_scripts.utils.client import create_telegram_client

logger = logging.getLogger(__name__)


@click.command(name="list_users")
@click.option(
    "--session-name",
    "-s",
    help=(
        "Defines session name to persist the the tokens after "
        "successful auth and cache some requests"
    ),
    default="scripts.default",
    show_default=True,
)
@click.pass_context
async def main(
    ctx: click.Context,
    session_name: str,
) -> None:
    """List all available contact (users, channels, groups, etc.)"""
    async with create_telegram_client(
        session_name, ctx.obj["api_id"], ctx.obj["api_hash"], flood_sleep_threshold=3600
    ) as client:
        client: TelegramClient  # type: ignore

        for dialog in await client.get_dialogs():

            if isinstance(dialog.entity, types.Channel):
                logger.info(
                    f"Channel: {dialog.entity.title} (id={dialog.entity.id}; "
                    f"participants_count={dialog.entity.participants_count}; "
                    f"date={dialog.entity.date}"
                    + (
                        f"; link=https://t.me/{dialog.entity.username}"
                        if dialog.entity.username is not None
                        else ""
                    )
                )

            elif isinstance(dialog.entity, types.User):
                __represent_user(dialog.entity)

            elif isinstance(dialog.entity, types.Chat):
                logger.info(
                    f"Chat: {dialog.entity.title} (id={dialog.entity.id}; "
                    f"participants_count={dialog.entity.participants_count}; "
                    f"date={dialog.entity.date}"
                )

                for participant in await client.get_participants(
                    dialog.entity, aggressive=True
                ):
                    __represent_user(participant, prefix="\t - ")


def __represent_user(user: types.User, prefix: str = "") -> None:
    full_name = (
        f'{(user.first_name or "")} {(user.last_name or "")}'
    ).strip() or "<Unknown Name>"

    logger.info(
        f"{prefix}User: {full_name} (id={user.id}; username={user.username};"
        f" phone={user.phone}; mutual_contact={user.mutual_contact}; bot={user.bot};"
        f" lang_code={user.lang_code}"
    )
