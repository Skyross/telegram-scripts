# -*- coding: utf-8 -*-
import logging

import asyncclick as click
from more_itertools import chunked
from telethon import TelegramClient, events
from telethon.utils import get_display_name

from telethon_scripts.utils.client import create_telegram_client

# telegram client allows to select only this number of messages, for now
CHUNK_SIZE = 100

logger = logging.getLogger(__name__)


@click.command(name="wipe_chat")
@click.option(
    "--session-name",
    "-s",
    help=(
        "Defines session name to persist the the tokens after "
        "successful auth and cache some requests"
    ),
    default="scripts.wipe_chat",
    show_default=True,
)
@click.pass_context
async def main(
    ctx: click.Context,
    session_name: str,
) -> None:
    """Script to wipe message. Need to login before usage."""
    async with create_telegram_client(
        session_name, ctx.obj["api_id"], ctx.obj["api_hash"], flood_sleep_threshold=3600
    ) as client:
        client: TelegramClient  # type: ignore

        @client.on(events.NewMessage(pattern="/wipe"))
        async def handler(event: events.NewMessage) -> None:
            chat_description = (
                f'id={event.chat_id} name="{get_display_name(event.chat)}" '
                f"sender_id={event.message.sender_id}"
            )

            logger.info("Wipe request received for chat: " + chat_description)

            message_ids = []
            async for message in client.iter_messages(event.chat_id):
                message_ids.append(message.id)

            for chunk in chunked(message_ids, CHUNK_SIZE):
                await client.delete_messages(event.chat_id, chunk, revoke=True)

            logger.info("Wipe request completed for chat: " + chat_description)

        await client.run_until_disconnected()
