# -*- coding: utf-8 -*-
import asyncclick as click
from alchemysession.orm import AlchemySession

from telethon_scripts.utils.client import create_telegram_client


@click.command(name="login")
@click.option(
    "--session-name",
    "-s",
    prompt=True,
    help="Session name for Telethon cache persistence.",
)
@click.pass_context
async def main(ctx: click.Context, session_name: str) -> None:
    """
    Method to login and create persistence for future API requests.
    """
    async with create_telegram_client(
        session_name, ctx.obj["api_id"], ctx.obj["api_hash"]
    ) as client:
        # noinspection PyUnresolvedReferences
        session: AlchemySession = client.session
        session.save()
