# -*- coding: utf-8 -*-
import asyncio
import enum
import logging
from pathlib import Path
from typing import Tuple, cast

import asyncclick as click
from more_itertools import chunked
from telethon import TelegramClient
from telethon.errors import UserNotParticipantError
from telethon.tl.functions.channels import JoinChannelRequest, LeaveChannelRequest
from telethon.tl.types import TypeInputChannel

from telethon_scripts.utils.client import create_telegram_client

CHUNK_SIZE = 10

logger = logging.getLogger(__name__)


class Modes(str, enum.Enum):
    JOIN = "join"
    LEAVE = "leave"


@click.command(name="manage_channels")
@click.option(
    "--session-name",
    "-s",
    help=(
        "Defines session name to persist the the tokens after "
        "successful auth and cache some requests"
    ),
    default="scripts.manage_channels",
    show_default=True,
)
@click.option(
    "--mode",
    "-m",
    required=True,
    type=click.Choice([Modes.JOIN, Modes.LEAVE], case_sensitive=False),
    help="Mode to control the channels.",
)
@click.option(
    "--channel",
    "-c",
    "channels",
    multiple=True,
    help=(
        "Channel to subscribe/unsubscribe in a form of link or name. "
        "Supports many options."
    ),
)
@click.option(
    "--channel-list",
    "-cl",
    default="channels.txt",
    type=click.Path(exists=False),
)
@click.pass_context
async def main(
    ctx: click.Context,
    session_name: str,
    mode: Modes,
    channels: Tuple[str],
    channel_list: str,
) -> None:
    """Script to manage channel subscription. Need to login before usage."""
    channels_to_manage = set(channels) if channels else set()

    if Path(channel_list).exists():
        for line in Path(channel_list).read_text().split("\n"):
            channel_nickname = line.strip().replace("https://t.me/", "")
            if channel_nickname and not channel_nickname.startswith("#"):
                channels_to_manage.add(channel_nickname)

    if not channels_to_manage:
        raise click.BadOptionUsage(
            "--channel/--channel-list", "No channels were provided"
        )

    async with create_telegram_client(
        session_name, ctx.obj["api_id"], ctx.obj["api_hash"], flood_sleep_threshold=3600
    ) as client:
        await client.get_dialogs()

        for chunk in chunked(channels_to_manage, CHUNK_SIZE):
            await asyncio.gather(
                *(
                    caller(client, mode, channel_nickname=channel_nickname)
                    for channel_nickname in chunk
                ),
            )


async def caller(client: TelegramClient, mode: Modes, channel_nickname: str) -> None:
    method = JoinChannelRequest if mode == Modes.JOIN else LeaveChannelRequest

    try:
        await client(method(channel=cast(TypeInputChannel, channel_nickname)))

    except UserNotParticipantError:
        logger.warning(f"Already left from {channel_nickname}")

    else:
        logger.info(
            f'{"Joined to" if mode == Modes.JOIN else "Left from"} {channel_nickname}'
        )
