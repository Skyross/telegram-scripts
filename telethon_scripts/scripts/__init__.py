# -*- coding: utf-8 -*-
import asyncclick as click

from .list_users import main as list_users
from .login import main as login
from .manage_channels import main as manage_channels
from .takeout_media import main as takeout_media
from .wipe_chat import main as wipe_chat


@click.group()
@click.option(
    "--api-id", "-i", type=int, prompt=True, envvar="TELEGRAM_API_ID", show_envvar=True
)
@click.option(
    "--api-hash", "-h", prompt=True, envvar="TELEGRAM_API_HASH", show_envvar=True
)
@click.pass_context
async def main(ctx: click.Context, api_id: int, api_hash: str) -> None:
    """
    Utility to manage Telegram scripts.
    """
    # ensure that ctx.obj exists and is a dict (in case `cli()` is called
    # by means other than the `if` block below)
    ctx.ensure_object(dict)

    ctx.obj["api_id"] = api_id
    ctx.obj["api_hash"] = api_hash


main.add_command(list_users)
main.add_command(login)
main.add_command(manage_channels)
main.add_command(takeout_media)
main.add_command(wipe_chat)
