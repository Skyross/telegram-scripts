# -*- coding: utf-8 -*-
import logging

import asyncclick as click
from telethon import TelegramClient, errors
from telethon.tl.patched import Message

from telethon_scripts.utils.client import create_telegram_client

logger = logging.getLogger(__name__)


@click.command(name="takeout_media")
@click.option(
    "--session-name",
    "-s",
    help=(
        "Defines session name to persist the the tokens after "
        "successful auth and cache some requests"
    ),
    default="scripts.takeout_media",
    show_default=True,
)
@click.option(
    "--chat-id",
    "-i",
    type=int,
    prompt=True,
    help=(
        "Channel ID to download media from. You may obtain ID using 'list_users'"
        " command"
    ),
)
@click.option(
    "--output",
    "-o",
    type=click.Path(exists=True, writable=True),
    default="downloads",
    show_default=True,
    help="Output folder to download media to.",
)
@click.pass_context
async def main(
    ctx: click.Context,
    session_name: str,
    chat_id: int,
    output: str,
) -> None:
    """Script to take media out from the channel. Need to login before usage."""
    async with create_telegram_client(
        session_name, ctx.obj["api_id"], ctx.obj["api_hash"], flood_sleep_threshold=3600
    ) as client:
        client: TelegramClient  # type: ignore

        chat = None
        for dialog in await client.get_dialogs():
            if dialog.entity.id == chat_id:
                chat = dialog.entity
                break

        if chat is None:
            raise RuntimeError(f"Unable to find the chat with ID={chat_id}")

        try:
            async with client.takeout() as takeout:
                async for message in takeout.iter_messages(chat, wait_time=0):
                    message: Message  # type: ignore

                    if message.media is not None:
                        filepath = await message.download_media(output)
                        logger.info(f"Media downloaded to {filepath}")

        except errors.TakeoutInitDelayError as e:
            logger.warning(f"Must wait {e.seconds} before takeout.")
