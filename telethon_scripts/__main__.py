#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging

import asyncclick as click

from telethon_scripts.bots import main as bots
from telethon_scripts.scripts import main as scripts


@click.group()
@click.option("-v", "--verbose", count=True)
@click.pass_context
async def cli(ctx: click.Context, verbose: int) -> None:
    # ensure that ctx.obj exists and is a dict (in case `cli()` is called
    # by means other than the `if` block below)
    ctx.ensure_object(dict)

    # Enable logging
    level = max(logging.INFO - verbose * 10, logging.DEBUG)
    logging.basicConfig(
        format="[%(levelname)7s] - %(asctime)s - %(name)s: %(message)s", level=level
    )


cli.add_command(bots, name="bots")
cli.add_command(scripts, name="scripts")

if __name__ == "__main__":
    cli(obj={}, _anyio_backend="asyncio", auto_envvar_prefix="TELEGRAM")
