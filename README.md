# Telegram scripts

## Setup

Retrieve [`app_id` and `app_hash`][0] values from your personal record at [Telegram core][1].

Create `.env` with the secrets (see `.env-base`):

    COMPOSE_PROJECT_NAME=telethon_scripts

    TELEGRAM_API_ID=...
    TELEGRAM_API_HASH=...

    POSTGRES_HOST=...
    POSTGRES_PORT=...
    POSTGRES_DB=...
    POSTGRES_USER=...
    POSTGRES_PASSWORD=...

Up the PostgreSQL:

    docker-compose up postgres

Login into your account with your personal credentials in order to run scripts that access Telegram's User API:

    docker-compose run wipe_chat scripts login --session-name=scripts.wipe_chat

... and follow the instructions.

**Note:** pay attention to `--session-name=...`, it used to have different sessions for different scripts and bots in
order to ease the management. You need to log in into each script/bot that you want to use.

Run up required containers:

    docker-compose up --remove-orphans wipe_chat

[0]: https://core.telegram.org/api/obtaining_api_id#obtaining-api-id

[1]: https://my.telegram.org/

## How to develop

Python 3.9 is the minimum version to develop.

    pip install -U pip poetry==1.1.13
    poetry install
    pre-commit install

## How to update dependencies

    poetry update

## direnv

The handful way to load your `.env` values is using [direnv][2], do not forget to run this after it's installation and
setup:

    direnv allow

[2]: https://direnv.net/

## Build

Verified on macOS (Apple Silicon).

Create a new builder and check if it is selected:

```shell
docker buildx build \
    --platform linux/arm64,linux/amd64 \
    -t skyross/telegram-scripts:latest --push \
    .
```
