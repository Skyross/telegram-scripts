FROM python:3.9-slim AS base

ENV POETRY_NO_INTERACTION=1 \
    POETRY_VERSION=1.1.13 \
    POETRY_VIRTUALENVS_CREATE=false \
    PYTHONPATH=.

WORKDIR /opt/app


FROM base AS builder

RUN apt-get update \
    && apt-get upgrade -y --no-install-recommends \
    && apt-get install -y --no-install-recommends \
    build-essential=12.9 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install python required packages
RUN pip install --no-cache-dir pip==22.0.3 pex==2.1.70 "poetry==${POETRY_VERSION}" \
    && poetry --version

# Prepare dependencies
COPY poetry.lock pyproject.toml ./
RUN poetry export --without-hashes -f requirements.txt -o requirements.txt \
    && pip wheel -w wheelhouse -r requirements.txt

# Build PEX file from sources
COPY telethon_scripts telethon_scripts
RUN pex -vvv \
    -r requirements.txt \
    -f wheelhouse \
    --compile \
    --disable-cache \
    --no-build \
    --no-index \
    --sources-directory=. \
    --use-system-time \
    --wheel \
    -o /service.pex \
    && chmod +x /service.pex


FROM base AS final

COPY --from=builder /service.pex ./

ENTRYPOINT ["./service.pex", "-m", "telethon_scripts"]
